package com.mikea.tempconverter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "conversionServlet", value = "/convert")
public class TempConverter extends HttpServlet {

    public static double celsiusToFahrenheit(double val) {
        return val * 9 / 5 + 32;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        double inputTemp = Double.parseDouble(request.getParameter("temp"));

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>Results</h1>");

        out.print("<p><b>Your temperature: " + inputTemp + " celsius</b></p>");
        out.println("<p> Temp in fahrenheit: " + celsiusToFahrenheit(inputTemp) + "</p>");

        out.println("</body></html>");
    }
}
